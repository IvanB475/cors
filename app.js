const express = require('express');
const cors = require('cors');
const app = express();

// use cors as middleware, enable all origins for all routes
app.use(cors());

//define specific whitelisted origins
const corsOptions = {
    origin: 'http://example.com'
};

// register a get route that only has middleware-defined cors
app.get('/', (req, res) => {
    res.send("This is allowed since cors accepts any origin")
});

// register a get route that uses route-level cors configuration
app.get('/cors-test', cors(corsOptions), (req, res) => {
    res.send("This will fail because only one origin is whitelisted");
});


app.listen(8000);